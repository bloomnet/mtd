package florist;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;

import java.security.Permission;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import au.com.bytecode.opencsv.CSVReader;
import org.apache.log4j.Logger;

public abstract class ImportFloristData {
	
	public static Logger logger = Logger.getLogger(ImportFloristData.class);
	public static final String DATE_FORMAT_NOW = "yyyyMMddHHmmss";
	private static final String ERROR_MSG="You must specify a type of import to run. \nOptions are:\n" +
			"\tlegacy:\t Original process (no Excel processing)\n" +
			"\tmtd:    \tMTD sending (record type Account)\n" +
			"\tcsh:    \tCSH Summary \n" +
			"\tcp:    \tContractPal Integration\n\n";
	
	protected String useProxy;
	protected String proxyHost;
	protected String proxyPort;
	protected String proxyUsername;
	protected String proxyPassword;
	
	
	public static void main(String[] args){

		if(args==null || args.length<1){
			logger.error(ERROR_MSG);
			System.exit(1);
		}

		ImportFloristData importProcess = null;
		if("legacy".equalsIgnoreCase(args[0]))
		{
			importProcess = new LegacyProcessing();
			importProcess.process();
		}
		else if("mtd".equalsIgnoreCase(args[0])){
			
			importProcess =  new MTDSendingProcess();
			importProcess.process();
		}
		else if("csh".equalsIgnoreCase(args[0])){
			
			importProcess =  new CSHSummaryProcess();
			importProcess.process();
		}
		else if("blk".equalsIgnoreCase(args[0])){
			
			importProcess =  new BloomlinkProcessing();
			importProcess.process();
		}else if("cp".equalsIgnoreCase(args[0])){
			
			importProcess =  new ContractPalProcessing();
			importProcess.process();
		}else{
			System.out.println(ERROR_MSG);
		}
		
	}
	
	public abstract void process();
	
	//Format the ZipCode in 00000 format
	public static String formatZip(String zipCode) 
	{
		try 
		{
			if((zipCode.length() == 5) || (zipCode == ""))
			{
				return zipCode;
			}
			while(zipCode.length() < 5) 
			{
				zipCode = "0" + zipCode;
			}
		} catch (Exception e) {
			logger.error("Exception in formatZip()" + e.getMessage());
		}
		return zipCode;
	}
	
	//Format the Date in MM/dd/yyyy format
	public static String formatDate(String date)
	{
		try
		{
		    if(!date.equals(""))
		    {
				DateFormat informat = new SimpleDateFormat("dd-MMM-yy");
				DateFormat outformat = new SimpleDateFormat("MM/dd/yyyy");
				date=outformat.format(informat.parse(date));
		    }
		}
		catch(Exception e)
		{
			logger.error("Exception in formatDateEnteredColumn()" + e.getMessage());
		}
		return date;
	}
	
	//Reads given mapping file
	public static HashMap ReadMapFile(String strMapFile) throws Exception
	{
		HashMap hshMapData=null;
		try{
			hshMapData=new HashMap();
			CSVReader flStatusreader = new CSVReader(new FileReader(strMapFile));
	  		String [] nextLine;
	  		flStatusreader.readNext();
	  		while ((nextLine = flStatusreader.readNext()) != null) { 
	  			hshMapData.put(nextLine[0],nextLine[1]);
	  		}
		}
		catch(Exception e)
		{
			throw new Exception("Exception in Reading MappingFile for " + strMapFile +": "+  e.getMessage());
		}
		return hshMapData;
	}
		
	public static String getCurrentDate() 
	{
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		return sdf.format(cal.getTime());
	}

	protected static int getYearFromDate(Date date) {
	    int result = -1;
	    if (date != null) {
	        Calendar cal = Calendar.getInstance();
	        cal.setTime(date);
	        result = cal.get(Calendar.YEAR);
	    }
	    return result;
	}
	
	/** Our custom ExitManager does not allow the VM
	to exit, but does allow itself to be replaced by
	the original security manager.
	@author Andrew Thompson */
	class ExitManager extends SecurityManager {

	  SecurityManager original;

	  ExitManager(SecurityManager original) {
	     this.original = original;
	  }

	  /** Deny permission to exit the VM. */
	   public void checkExit(int status) {
	     throw( new SecurityException() );
	  }

	  /** Allow this security manager to be replaced,
	  if fact, allow pretty much everything. */
	  public void checkPermission(Permission perm) {
	  }

	  public SecurityManager getOriginalSecurityManager() {
	     return original;
	  }
	}
	
	protected void sendData(String recType , String operation, String username, String pwd, String dataFilePath, String mapFilePath, String hosturl, final File completeDir, boolean updateWithRecordId){
		/** String [] args2 = {"-operation","update","-recordtype","Account","-username",
				"bloomnet/integration","-datafilepath", "sample/09_17_2012_MTD.csv", "-mapfilepath",
				"sample/Demand_Account_Field_Mapping.map","-clientloglevel","detailed","-waitforcompletion","True"
				,"-importloglevel","all","-hosturl","https://secure-ausomxaoa.crmondemand.com"};
		**/
		String[] args2 = null;
		
		if(updateWithRecordId == true){
			String[] args3 = {"-operation",operation,"-recordtype",recType,"-username",
					username,"-datafilepath", dataFilePath, "-mapfilepath",
					mapFilePath,"-clientloglevel","all","-waitforcompletion","True"
					,"-importloglevel","all","-hosturl",hosturl,"-duplicatecheckoption","rowid"};
			args2 = args3;
		}else{
			String[] args3 = {"-operation",operation,"-recordtype",recType,"-username",
					username,"-datafilepath", dataFilePath, "-mapfilepath",
					mapFilePath,"-clientloglevel","all","-waitforcompletion","True"
					,"-importloglevel","all","-hosturl",hosturl};
			args2 = args3;
		}
		
		String [] argsToSend = null;
		InputStream i = null;
		if(useProxy.equalsIgnoreCase("true")){
			
			argsToSend = new String[args2.length+6];
			//argsToSend = new String[args2.length];
			int c = 0;
			for(String a : args2){
				argsToSend[c] = a;
				c = c+1;
			}
			
			//now add proxy parms
			argsToSend[c++] = "-proxyserver";
			argsToSend[c++] = proxyHost;
			argsToSend[c++] = "-proxyserverport";
			argsToSend[c++] = proxyPort;
			argsToSend[c++] = "-proxyauthusername";
			argsToSend[c++] = proxyUsername;
			
			//Added per NTLM issue 6/7/2013
			
		    //System.setProperty("https.proxyHost", proxyHost);
		    //System.setProperty("https.proxyPort", proxyPort);
		    //System.setProperty("proxyHost", proxyHost);
		    //System.setProperty("proxyPort", proxyPort);
		    //System.setProperty("proxySet", "true");
		    //System.setProperty("http.proxyHost", proxyHost);
		    //System.setProperty("http.proxyPort", proxyPort);
		    ///System.setProperty("http.proxySet", "true");
		   System.getProperties().put("http.proxyUser", proxyUsername);
		   System.getProperties().put("http.proxyPassword", proxyPassword);
		    
			i= new ByteArrayInputStream((pwd+"\n"+proxyPassword).getBytes());
			//i= new ByteArrayInputStream((pwd).getBytes());
		}else{
			argsToSend = args2;
			i= new ByteArrayInputStream((pwd).getBytes());
		}
		
		
		
		OutputStream os = new ByteArrayOutputStream();
		PrintStream o = new PrintStream(os);
		System.setIn(i);
		
		final PrintStream oldOut = System.out;
		PrintStream oldErr = System.err;


		final PrintStream ps = new PrintStream(new ByteArrayOutputStream()){

			@Override
			public void println(String x) {
				String x1 = x;
				//System.err.println("@@@"+x+"@@@ "+x.indexOf("Press CTRL-C to shutdown"));
				
				if(x!=null && (x.indexOf("Press CTRL-C to shutdown")>-1)){
					//logger.debug("CRM On Demand upload complete.");
					//System.err.println("!!!!!!   DONE  !!!!!!");
					//bulkOperations.Terminator.terminate();
					//System.setOut(oldOut);
					System.out.println("\n\nPress CTRL-C to shutdown");
					//this.flush();
					//this.close();
					//oldOut.flush();
					//System.exit(0);
					//bulkOperations.Terminator.terminate();
					//BulkOpsClientShutdownHandler.getInstance().signalReadyToExit();
					
					return;
					
				}
				//System.err.println(x);
				try {
					oldOut.write((x+"\n").getBytes());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				logger.debug(x);
				//super.println("@@@"+x+"@@@\n");
			}
			

			
		};
		System.setOut(ps);
		BulkOpsClient.main(argsToSend);		
			
		
		

		
		
	}
	
	public static void main2(String [] args){
		


	       
	       InputStream i = new DataInputStream(new ByteArrayInputStream("in1\ntes1".getBytes())){

   	   
	    	   
	       };
	       System.setIn(i);
	       Scanner in = new Scanner(System.in);

	       // Reads a single line from the console 
	       // and stores into name variable
	       String name = in.nextLine();

	       // Reads a integer from the console
	       // and stores into age variable
	       String age=in.nextLine();
	       in.close();            

	       // Prints name and age to the console
	       System.out.println("Name :"+name);
	       System.out.println("Age :"+age);
			}
	
	
	protected void moveFiles(String [] filesToMove, String completeDir){
		
		for(String f : filesToMove){
			
			File completedFileName = new File(f);
	        boolean isMoved=completedFileName.renameTo(new File(completeDir,completedFileName.getName()));
	        if (isMoved) {
	            logger.debug(" The file "+ completedFileName.getName() + " was processed and moved to completed folder");
	        }
	        else{
	        	logger.error(" Failed to move the processsed file: "+ completedFileName.getAbsolutePath());
	        }
		}
}
}
