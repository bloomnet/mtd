package florist;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;
import org.apache.log4j.PropertyConfigurator;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class LegacyProcessing extends ImportFloristData{
	
	

	
	/*private void logout() throws IOException{
		
		HttpsURLConnection myURLConnection = (HttpsURLConnection) createConnection(logoutURL,"GET");
		try {
			myURLConnection.setSSLSocketFactory(new TLSSocketFactory());
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		myURLConnection.connect();
	} */

	/*private HttpsURLConnection createConnection(String URL, String type) throws IOException{
		
		Authentication auth = new Authentication();
		try {
			cookieString = auth.login();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		URL myURL = new URL(URL);
		HttpsURLConnection myURLConnection = (HttpsURLConnection)myURL.openConnection();
		try {
			myURLConnection.setSSLSocketFactory(new TLSSocketFactory());
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		myURLConnection.setRequestProperty ("Cookie", cookieString);
		myURLConnection.setRequestMethod(type);
		myURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		myURLConnection.setRequestProperty("Content-Language", "en-US");
		myURLConnection.setUseCaches(false);
		myURLConnection.setDoInput(true);
		myURLConnection.setDoOutput(true);
		
		return myURLConnection;
	}*/
	
	/*private String getUser(String shopCode) throws IOException{
		
		HttpsURLConnection myURLConnection = createConnection(serviceURL + "'"+shopCode+"'", "GET");
		try {
			myURLConnection.setSSLSocketFactory(new TLSSocketFactory());
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		myURLConnection.connect();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
		String response = br.readLine();
		
		String macCode = response.split("\"OwnerId\":\"")[1].split("\"")[0];
		logger.debug("MAC Code Found: " + macCode);
		
		return macCode;
		
	}*/
	
	public void process(){
		
		HashMap inputFileData =null;
		HashMap mapFields=null;
		//Session objSession;
		//Florist florist;
		//Users users;
		
		try{
			//Configure Log details
			PropertyConfigurator.configure("log4j.properties");
			logger.debug("Process Started...");
			
			//Reading properties file
			HashMap loadProperties = ReadProperties.ReadPropertiesFile("BloomnetFlorist.properties");
			String FileDelimiter=loadProperties.get("FileDelimiter").toString();
			
			String UploadFileLocation=loadProperties.get("UploadFileLocation").toString();
			String ErrorFileLocation=loadProperties.get("ErrorFileLocation").toString();
			String CompletedFileLocation=loadProperties.get("CompletedFileLocation").toString();
			File uploadDir = new File(UploadFileLocation);
			File errorDir = new File(ErrorFileLocation);
			File completeDir = new File(CompletedFileLocation);
			
			//Checking input file folder
  			if(!uploadDir.exists()){
				throw new Exception("Upload folder do not exist");	
			}
  			//Checking input file existance
  			String[] UploadFiles = uploadDir.list();
    		if(UploadFiles.length <= 0)
    		{
    			throw new FileNotFoundException();
    		}
    		logger.debug("Number of input files required processing: " +UploadFiles.length);
    		//Checking for Completed file folder 
			if(!completeDir.exists()){
				completeDir.mkdir();	
			}
			//Checking for Error file folder 
			if(!errorDir.exists()){
				errorDir.mkdir();
			}				
			//Creating Error file
			File errorFile=new File(errorDir.getPath()+"/"+"Error"+getCurrentDate()+".txt");
		    if(!errorFile.exists())
		    {
		    	errorFile.createNewFile();
		    	logger.debug("Error file has been created " +errorFile.getPath());
		    }
		    CSVWriter errWriter = new CSVWriter(new FileWriter(errorFile),'\t');
		    
			//Read all mapping files
			String floristClassFile= "MappingFiles/" + "FloristClass.csv";
			String floristStatusFile= "MappingFiles/" + "FloristStatus.csv";
			String membershipFile= "MappingFiles/" + "Membership.csv";
			String territoryFile= "MappingFiles/" + "Territory.csv";
			String countryCodeFile= "MappingFiles/" + "Country.csv";
			String countryCodeFile2= "MappingFiles/" + "Country2.csv";
			String phoneTypeCodeFile= "MappingFiles/" + "Phone_Type.csv";
  			HashMap hshFloristClass=ReadMapFile(floristClassFile);
  			HashMap hshFloristStatus=ReadMapFile(floristStatusFile);
  			HashMap hshMembeship=ReadMapFile(membershipFile);
  			HashMap hshTerritory=ReadMapFile(territoryFile);  			
  			HashMap hshCountryCode=ReadMapFile(countryCodeFile);
  			HashMap hshCountryCode2=ReadMapFile(countryCodeFile2);
  			HashMap hshPhoneTypeCode=ReadMapFile(phoneTypeCodeFile);
  			
  			//Read Mapping fields from file
  		    String mapFieldFileName= "MappingFiles/" + "FLD-Florists.map";
  			CSVReader MapFilereader = new CSVReader(new FileReader(mapFieldFileName));
  			mapFields=new HashMap();
  			String[] nextField;
	  		MapFilereader.readNext();
	  		while ((nextField = MapFilereader.readNext()) != null) {
	  			mapFields.put(nextField[1].replaceAll("\"",""),nextField[0].replaceAll("\"",""));
	  		}
			
	  		//Creating objects
			//florist=new Florist();
			//users=new Users();
				
			//Creating sessions
			//objSession = new Session();
			//Checking if proxy is set
			//if(objSession.getUseProxy().equalsIgnoreCase("true"))
			//{
			//	objSession.EstablishWithProxy();
		//	}
		//	else
			//{
			//	objSession.Establish();	
			//}
			//logger.debug("Created CRMOD Session");
			
			//Setting sessions to objects
			//florist.setSession(objSession);
			//users.setSession(objSession);	
			
			//Get Users list
			//HashMap hshUserList = users.QueryMacNames();
			
			for (int i=0; i<UploadFiles.length; i++) 
			{   
		        String inputFileName= UploadFileLocation + "/" + UploadFiles[i];
		        logger.debug("Processing input file name: " + inputFileName);
				BufferedReader br = new BufferedReader(new FileReader(inputFileName));
	  			String[] strAryHeader=br.readLine().split(FileDelimiter);
	  			if(i == 0)
	  			{
	  			errWriter.writeNext(strAryHeader);
	  			}
	  			String nextRow;
	  			
	  			//Read each row from the input file and insert it to CRM
	  			while((nextRow = br.readLine())!= null)
	  			{
	  				String[] strAryRow = nextRow.split(FileDelimiter);
	  				inputFileData=new HashMap();
	    			for(int row=0; row<strAryHeader.length; row++)
	  				{
	    				try
	    				{
	    					inputFileData.put(strAryHeader[row],strAryRow[row]);
	    				}
	    				catch(ArrayIndexOutOfBoundsException ar)
	    				{
	    					inputFileData.put(strAryHeader[row],"");
	    				}
	  				}
	    			inputFileData.put("Province","");
	    			inputFileData.put("Country","");    			
	    			
	    			try
	    			{
		    			//Validating required fields
		    			logger.debug("Processing the record for the Shop Code: " + inputFileData.get("Shop Code").toString());
		    			
		    			inputFileData.put("Zip",formatZip(inputFileData.get("Zip").toString()));		    			inputFileData.put("Date entered",formatDate(inputFileData.get("Date entered").toString()));
			  			inputFileData.put("Date entered",formatDate(inputFileData.get("Date entered").toString()));
			  			inputFileData.put("Direct Pay Start Date",formatDate(inputFileData.get("Direct Pay Start Date").toString()));
		    			
		    			String strFlClass="";
			  			if(!inputFileData.get("Florist Class").equals(null) && !inputFileData.get("Florist Class").equals(""))
		    			{
			  				try{
			    			strFlClass=hshFloristClass.get(inputFileData.get("Florist Class")).toString();
			  				}
			  				catch (Exception e)
			  				{
			  					throw new Exception("Error while validating the Florist Class :"+inputFileData.get("Florist Class").toString() + "  Error: " + e.getMessage());
			  				}		 
			    			inputFileData.put("Florist Class",strFlClass);
		    			}
			  			String strMembership="";
			  			if(!inputFileData.get("Membership").equals(null) && !inputFileData.get("Membership").equals(""))
		    			{
			  				try{
			  					strMembership=hshMembeship.get(inputFileData.get("Membership")).toString();
			  				}
			  				catch (Exception e)
			  				{
			  					throw new Exception("Error while validating the Membership :"+inputFileData.get("Membership").toString() + "  Error: " + e.getMessage());
			  				}		  				
		    				inputFileData.put("Membership",strMembership);
		    			}
			  			if(!inputFileData.get("Territory").equals(null) && !inputFileData.get("Territory").equals(""))
		    			{
			    			if(!hshTerritory.containsValue(inputFileData.get("Territory")))
			    			{
			    				throw new Exception("Error while validating the Territory :"+inputFileData.get("Territory").toString());
			    			}		    		
		    			}
			  			String strFlStatus="";
		    			if(!inputFileData.get("Florist Status").equals(null) && !inputFileData.get("Florist Status").equals("") )
		    			{
		    				try{
		    				strFlStatus=hshFloristStatus.get(inputFileData.get("Florist Status")).toString();
			    			}
			  				catch (Exception e)
			  				{
			  					throw new Exception("Error while validating the Florist Status :"+inputFileData.get("Florist Status").toString() + "  Error: " + e.getMessage());
			  				}		 
			  				inputFileData.put("Florist Status",strFlStatus);
		    			}
		    			String strCountry = "";
		    			String state = inputFileData.get("State").toString();
		    			if(state != null && !state.equals("") )
		    			{
		    				
	    					if(hshCountryCode.get(state) != null){
	    						strCountry = hshCountryCode.get(state).toString();
	    					}else if(hshCountryCode2.get(inputFileData.get("Zip")) != null){
	    						System.out.println("No Country Found For State: "+state);
	    						System.out.println("Zip: "+inputFileData.get("Zip").toString());
	    						strCountry = hshCountryCode2.get(inputFileData.get("Zip")).toString();
		    				}else{
		    					throw new Exception("Error while validating the State :"+ state);
	    					}
				    		
		    				inputFileData.put("Country",strCountry);
		    				if(!strCountry.equals("USA"))
		    				{
		    					inputFileData.put("Province",inputFileData.get("State").toString());
		    					inputFileData.put("State", "");
		    				}
		    			}
		    			String strPhoneType1="";
		    			if(!inputFileData.get("PhoneType1").toString().equals(null) && !inputFileData.get("PhoneType1").toString().equals(""))
		    			{
			  				strPhoneType1=hshPhoneTypeCode.get(inputFileData.get("PhoneType1")).toString();
			  				inputFileData.put("PhoneType1",strPhoneType1);
		    			}
			  			String strPhoneType2="";
			  			if(!inputFileData.get("PhoneType2").toString().equals(null) && !inputFileData.get("PhoneType2").toString().equals(""))
		    			{
			  				strPhoneType2=hshPhoneTypeCode.get(inputFileData.get("PhoneType2")).toString();
			  				inputFileData.put("PhoneType2",strPhoneType2);
		    			}
			  			String strPhoneType3="";
			  			if(!inputFileData.get("PhoneType3").toString().equals(null) && !inputFileData.get("PhoneType3").toString().equals(""))
		    			{
			  				strPhoneType3=hshPhoneTypeCode.get(inputFileData.get("PhoneType3")).toString();
			  				inputFileData.put("PhoneType3",strPhoneType3);
		    			}
			  			
			  			String strPhoneType4="";
				  		if(!inputFileData.get("PhoneType4").toString().equals(null) && !inputFileData.get("PhoneType4").toString().equals(""))
			    		{
				  			strPhoneType4=hshPhoneTypeCode.get(inputFileData.get("PhoneType4")).toString();
				  			inputFileData.put("PhoneType4",strPhoneType4);
			    		}

			  			String strPhoneType5="";
			  			if(!inputFileData.get("PhoneType5").toString().equals(null) && !inputFileData.get("PhoneType5").toString().equals(""))
		    			{
			  				strPhoneType5=hshPhoneTypeCode.get(inputFileData.get("PhoneType5")).toString();
			  				inputFileData.put("PhoneType5",strPhoneType5);
		    			}
			  			
		    			
		    			//Create insert object
		    			/*Set keySet = inputFileData.keySet();
		    			Iterator it = keySet.iterator();
		    			ListOfAccount accountObj=new ListOfAccount();
	   				    Account[] acct= new Account[1];
	   				    acct[0] = new Account();
	   					while(it.hasNext())
		    			{
		    				String strField = it.next().toString();
		    				if(mapFields.containsKey(strField))
		    			    {
		    				  acct[0] = (Account) SetValueToObject(acct[0],mapFields.get(strField).toString(),inputFileData.get(strField).toString());
		    				  AccountStub.String15 currencyCode=new AccountStub.String15();
		    				  currencyCode.setString15("");
		    				  acct[0].setCurrencyCode(currencyCode);
		    				}
		    			}
		    			accountObj.setAccount(acct);
		    			florist.InsertOrUpdateAccountRecord(accountObj); */
		    			
	    			}
	    			catch(Exception ex)
	    			{
	    				logger.error(ex.getMessage());
	    				ex.printStackTrace();
	    				String[] error={ex.getMessage()};
	    				errWriter.writeNext(strAryRow);
	    				errWriter.writeNext(error);
	    				errWriter.flush();
	    			}
	    		} 
	  			
	  			br.close();
		        File completedFileName = new File(inputFileName);
		        createCSV(completedFileName.getPath());
		        boolean isMoved=completedFileName.renameTo(new File(completeDir,completedFileName.getName()));
		        if (isMoved) {
		            logger.debug(" The file "+ completedFileName.getName() + " was processed and moved to completed folder");
		        }
		        else{
		        	logger.error(" Failed to move the processsed file: "+ completedFileName.getName());
		        }
		    }
			
			//objSession.Destroy();
			errWriter.close();
			logger.debug("All the files have been processed succesfully");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			logger.error("Error in main: "+e.getMessage());
		}
		
	}
	
	public String createCSV (String fullPath){
		File newFile = null;
		try{	
			BufferedReader br = new BufferedReader(new FileReader(fullPath));
  			String nextRow;
  			newFile = new File("/home/bloomnet/public/ftpFiles/SalesForceExport/atlas.csv");
  			FileWriter writer = new FileWriter(newFile);
  			
  			//Read each row from the input file and insert it to CRM
  			while((nextRow = br.readLine())!= null)
  			{    			
    			try{
    				String line = "\""+nextRow.replaceAll("\"", "").replaceAll("\t","\",\"")+"\"";
    				String[] lineItems = line.split("\",\"");
    				if(lineItems.length < 45){
    					int diff = 45 - lineItems.length;
    					writer.append(line);
    					for(int ii=0; ii<diff; ++ii){
    						writer.append(",\"\"");
    					}
    					writer.append("\r\n");
    				}else if(lineItems.length > 45){
    					//do nothing
    				}else{
    					writer.append(line+"\r\n");
    				}
    			}catch(Exception ex){
    				logger.error(ex.getMessage());
    				ex.printStackTrace();
    			}
    		} 
  			
  			br.close();
  			writer.flush();
  			writer.close();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			logger.error("Error in main: "+e.getMessage());
		}
		
		return "/home/bloomnet/public/ftpFiles/SalesForceExport/" + newFile.getName();
		
	}
	
	
}
