package florist;

import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;

public class MailAutomation
{

    public MailAutomation(String subject, String messageText, String filePath, String fileName)
        throws MessagingException
    {
        String host = "smtp.gmail.com";
        String from = "bloomnetautomatedreports@gmail.com";
        String Password = "Bl00mn3t123";
        String toAddress = "msilver@bloomnet.net,jpursoo@bloomnet.net,cyearick@bloomnet.net";
        Properties props = System.getProperties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtps.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.port", "25");
        javax.mail.Session session = javax.mail.Session.getInstance(props, null);
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from));
        message.setRecipients(javax.mail.Message.RecipientType.TO, toAddress);
        message.setSubject(subject);
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setText(messageText);
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
        messageBodyPart = new MimeBodyPart();
        javax.activation.DataSource source = new FileDataSource(filePath);
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName(fileName);
        multipart.addBodyPart(messageBodyPart);
        message.setContent(multipart);
        try
        {
            Transport tr = session.getTransport("smtps");
            tr.connect(host, from, Password);
            tr.sendMessage(message, message.getAllRecipients());
            System.out.println("*** MAIL SENT ***");
            tr.close();
        }
        catch(SendFailedException sfe)
        {
            System.out.println(sfe);
        }
    }

}
