package florist;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import net.bloomnet.csv.ExcelToCSV;
import net.bloomnet.poi.util.SheetUtil;

import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;



public class CSHSummaryProcess extends ImportFloristData {

	@Override
	public void process() {
		ExitManager sm;
		
		try{
			//Configure Log details
			PropertyConfigurator.configure("log4j_csh.properties");
			logger.debug("Process Started...");
			
			//Reading properties file
			HashMap loadProperties = ReadProperties.ReadPropertiesFile("BloomnetFlorist.properties");
			String fileDelimiter=loadProperties.get("cshsummary.FileDelimiter").toString();
			
			String UploadFileLocation=loadProperties.get("cshsummary.UploadFileLocation").toString();
			String ErrorFileLocation=loadProperties.get("cshsummary.ErrorFileLocation").toString();
			String CompletedFileLocation=loadProperties.get("cshsummary.CompletedFileLocation").toString();
			loadProperties.get("MappingFilesLocation").toString();
			loadProperties.get("cshsummary.mapFile").toString();

			loadProperties.get("CRMODUrl").toString();
			loadProperties.get("CRMODUsername").toString();
			loadProperties.get("CRMODPassword").toString();
			
			useProxy = loadProperties.get("UseProxy").toString();
			proxyHost = loadProperties.get("ProxyHost").toString();
			proxyPort = loadProperties.get("ProxyPort").toString();
			proxyUsername = loadProperties.get("ProxyUser").toString();
			proxyPassword = loadProperties.get("ProxyPassword").toString();
			
			File uploadDir = new File(UploadFileLocation);
			File errorDir = new File(ErrorFileLocation);
			File completeDir = new File(CompletedFileLocation);
			
			//Checking input file folder
  			if(!uploadDir.exists()){
				throw new Exception("Upload folder does not exist: "+UploadFileLocation);	
			}
  			//Checking input file existance
  			String[] UploadFiles = uploadDir.list();
    		if(UploadFiles.length <= 0)
    		{
    			throw new FileNotFoundException();
    		}
    		logger.debug("Number of input files required processing: " +UploadFiles.length);
    		//Checking for Completed file folder 
			if(!completeDir.exists()){
				completeDir.mkdir();	
			}
			//Checking for Error file folder 
			if(!errorDir.exists()){
				errorDir.mkdir();
			}				
			//Creating Error file
			/**File errorFile=new File(errorDir.getPath()+"\\"+"Error"+getCurrentDate()+".txt");
		    if(!errorFile.exists())
		    {
		    	errorFile.createNewFile();
		    	logger.debug("Error file has been created " +errorFile.getPath());
		    }
		    CSVWriter errWriter = new CSVWriter(new FileWriter(errorFile),'\t');
		    **/
		    sm = new ExitManager( System.getSecurityManager() );
		     System.setSecurityManager(sm);
			boolean foundXLS = false;
			for (int i=0; i<UploadFiles.length; i++) 
			{   
				
				
		        String inputFileName= UploadFileLocation + UploadFiles[i];
		        String ext="";
		        int mid= inputFileName.lastIndexOf(".");
		        inputFileName.substring(0,mid);
		        ext=inputFileName.substring(mid+1,inputFileName.length()); 

		        if(!("XLS".equalsIgnoreCase(ext) || "XLSX".equalsIgnoreCase(ext))){
		        	continue;
		        }
		        foundXLS = true;
		        
		        
		        
		        logger.debug("Processing input file name: " + inputFileName);
				
		        String csvFileName = UploadFiles[i].substring(0,UploadFiles[i].lastIndexOf("."))+".csv";
		       
		        
		        FileInputStream fis = new FileInputStream(inputFileName);
		       
		        Workbook workbook = WorkbookFactory.create(fis);
	  			
		        fis.close();
		        
		        
		        Sheet sheet = workbook.getSheetAt(0);
		        SheetUtil.insertColumn(sheet, 0);
		        SheetUtil.insertColumn(sheet, 1);
		        SheetUtil.insertColumn(sheet, 4);
		        sheet.getRow(0).getCell(0).setCellValue("UKEY");
		        sheet.getRow(0).getCell(1).setCellValue("Report Date");
		        sheet.getRow(0).getCell(4).setCellValue("R SHOP CODE");
		        sheet.getRow(0).getCell(11).setCellValue("LAST PAYMENT DATE");
		        sheet.getRow(0).getCell(12).setCellValue("LAST PAYMENT");
		        
		        new Date();
		        
		        SheetUtil.copyColumns(sheet,5,4, true); //copy F
		        SheetUtil.setColumnToText(sheet, 1, getYearFromFileName(UploadFiles[i])+"/"+getMonthFromFileName(UploadFiles[i]), true);
		        SheetUtil.setColumnToIncrement(sheet, 0, "CHSM"+getYearFromFileName(UploadFiles[i])+getMonthFromFileName(UploadFiles[i]), true);
		        //SheetUtil.deleteColumn(sheet, 2);//remove the third column
		        SheetUtil.setColumnDateFormat(sheet, 11, "", true);
		        
		        //FileOutputStream fileout = new FileOutputStream(UploadFileLocation + "_"+UploadFiles[i]);
		        //workbook.write(fileout);
		        
		        //now create CSV
		        ExcelToCSV csvConverter = new ExcelToCSV(workbook,fileDelimiter,UploadFileLocation,csvFileName );
		        csvConverter.convertWorkbookToCSV();
		        logger.debug("created CSV: " + inputFileName);
		        
		        String [] filesToMove = {inputFileName};
		        moveFiles(filesToMove, CompletedFileLocation);
		        logger.debug("Going to upload data to CRM On Demand....");
		      try{
		        //sendData("CustomObject6","insert",user,userPwd, UploadFileLocation+csvFileName,mappingFile,wsURL,completeDir,false);
		        }catch(java.lang.SecurityException se){
		        	
		        }
		        logger.debug("Upload to CRM On Demand COMPLETE for file: "+inputFileName);
	  			
		        break;
		    }
			
			if(!foundXLS){
				logger.error("No XLS files found in folder: "+UploadFileLocation);
				System.out.println("No XLS files found in folder: "+UploadFileLocation);
			}
			//errWriter.close();
			System.out.println("DONE!");
			logger.debug("All the files have been processed succesfully");
			System.setSecurityManager(sm.getOriginalSecurityManager() );
			System.exit(0);

		}
		catch(Exception e)
		{
			e.printStackTrace();
			logger.error("Error in main: "+e.getMessage());
		}
		
		
	}
	
	public static void main(String [] args){
		
		System.out.println(getMonthFromFileName("BMT_Clearing_House_Volume_Repo_18121980"));
	}

	private static String getMonthFromFileName(String filename){
		
		String [] secs = filename.split("_");
		if(secs.length<6)
			throw new IndexOutOfBoundsException("Filename is not in proper format {BMT_Clearing_House_Volume_Repo_DDMMYYYY}");
	
		return secs[5].substring(2,4);
	}
	
	private static String getYearFromFileName(String filename){
		
		String [] secs = filename.split("_");
		if(secs.length<6)
			throw new IndexOutOfBoundsException("Filename is not in proper format {BMT_Clearing_House_Volume_Repo_DDMMYYYY}");
	
		return secs[5].substring(4,8);
	}
	
	private String getMonthFromDate(Date date, boolean pad) {
		    int result = -1;
		    if (date != null) {
		        Calendar cal = Calendar.getInstance();
		        cal.setTime(date);
		        result = cal.get(Calendar.MONTH);
		    }
		    
		    if(pad && (result+1)<10)
		    	return "0"+String.valueOf(result+1);
		    
		    return String.valueOf(result+1);
		
	}

}
