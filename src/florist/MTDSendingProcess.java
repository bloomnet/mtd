package florist;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.HashMap;
import org.apache.log4j.PropertyConfigurator;
import org.apache.tools.ant.taskdefs.Copyfile;

import bulkOperations.BulkOpsClientShutdownHandler;

public class MTDSendingProcess extends ImportFloristData {

	@Override
	public void process() {
		
		
		//ExitManager sm;
		
		try{
			//Configure Log details
			PropertyConfigurator.configure("log4j_mtd.properties");
			logger.debug("Process Started...");
			
			//Reading properties file
			HashMap loadProperties = ReadProperties.ReadPropertiesFile("BloomnetFlorist.properties");
			loadProperties.get("mtdsending.FileDelimiter").toString();
			
			String UploadFileLocation=loadProperties.get("mtdsending.UploadFileLocation").toString();
			String ErrorFileLocation=loadProperties.get("mtdsending.ErrorFileLocation").toString();
			String CompletedFileLocation=loadProperties.get("mtdsending.CompletedFileLocation").toString();
			String mappingFileCurrentYear = loadProperties.get("MappingFilesLocation").toString()+loadProperties.get("mtdsending.mapFile.currentyear").toString();
			String mappingFilePriorYear = loadProperties.get("MappingFilesLocation").toString()+loadProperties.get("mtdsending.mapFile.prioryear").toString();
			
			loadProperties.get("CRMODUrl").toString();
			loadProperties.get("CRMODUsername").toString();
			loadProperties.get("CRMODPassword").toString();
			
			useProxy = loadProperties.get("UseProxy").toString();
			proxyHost = loadProperties.get("ProxyHost").toString();
			proxyPort = loadProperties.get("ProxyPort").toString();
			proxyUsername = loadProperties.get("ProxyUser").toString();
			proxyPassword = loadProperties.get("ProxyPassword").toString();

			
			File uploadDir = new File(UploadFileLocation);
			File errorDir = new File(ErrorFileLocation);
			File completeDir = new File(CompletedFileLocation);
			
			//Checking input file folder
  			if(!uploadDir.exists()){
				throw new Exception("Upload folder do not exist: "+UploadFileLocation);	
			}
  			//Checking input file existance
  			String[] UploadFiles = uploadDir.list();
    		if(UploadFiles.length <= 0)
    		{
    			throw new FileNotFoundException();
    		}
    		logger.debug("Number of input files required processing: " +UploadFiles.length);
    		//Checking for Completed file folder 
			if(!completeDir.exists()){
				completeDir.mkdir();	
			}
			//Checking for Error file folder 
			if(!errorDir.exists()){
				errorDir.mkdir();
			}				
			//Creating Error file
			/**File errorFile=new File(errorDir.getPath()+"\\"+"Error"+getCurrentDate()+".txt");
		    if(!errorFile.exists())
		    {
		    	errorFile.createNewFile();
		    	logger.debug("Error file has been created " +errorFile.getPath());
		    }
		    CSVWriter errWriter = new CSVWriter(new FileWriter(errorFile),'\t');
		    **/
		    //sm = new ExitManager( System.getSecurityManager() );
		     //System.setSecurityManager(sm);
			
			for (int i=0; i<UploadFiles.length; i++) 
			{   
				
				String mappingFile = mappingFilePriorYear;
		        String inputFileName= UploadFileLocation + UploadFiles[i];
		        String fname="";
		        String ext="";
		        int mid= inputFileName.lastIndexOf(".");
		        fname=inputFileName.substring(0,mid);
		        ext=inputFileName.substring(mid+1,inputFileName.length()); 

		        //if(!("XLS".equalsIgnoreCase(ext) || "XLSX".equalsIgnoreCase(ext))){//asuft taken out on Aug 7, 2013
		        if(!("CSV".equalsIgnoreCase(ext) )){
		        	continue;
		        }
		        
		        
		        String currentYear = String.valueOf(getYearFromDate(new Date()));
		        System.out.println("Current year: "+currentYear);
		        
		        boolean currYear = false;
		        
		        if(fname.indexOf(currentYear)>-1)
		        {
		        	mappingFile = mappingFileCurrentYear;
		        	currYear = true;
		        	
		        }
		        
		        logger.debug("Processing input file name: " + inputFileName);
				
		        //String csvFileName = UploadFiles[i].substring(0,UploadFiles[i].lastIndexOf("."))+".csv";//asuft 08072013
		       String csvFileName = inputFileName;//asuft 08072013
		        
		       /**
		        FileInputStream fis = new FileInputStream(inputFileName);
		       
		        Workbook workbook = WorkbookFactory.create(fis);
	  			workbook.removeSheetAt(1);//remove the SQL sheet
		        fis.close();
		        
		        Sheet sheet = workbook.getSheetAt(0);
		        SheetUtil.deleteColumn(sheet, 2);//remove the third column
		        
		        
		        //FileOutputStream fileout = new FileOutputStream(inputFileName);
		        //workbook.write(fileout);
		        
		        //now create CSV
		        ExcelToCSV csvConverter = new ExcelToCSV(workbook,fileDelimiter,UploadFileLocation,csvFileName );
		        csvConverter.convertWorkbookToCSV();
		        logger.debug("Going to convert XLS file "+inputFileName);
		        logger.debug("created CSV : " + inputFileName);
		        **/
		        String [] filesToMove = {csvFileName};
		        moveFiles(filesToMove, CompletedFileLocation);
		        logger.debug("Going to upload data to CRM On Demand....");
		        try{
		        	logger.debug("Going upload CSV file: "+csvFileName+" with mapping: "+mappingFile);
		        	//sendData("Account","update",user,userPwd, CompletedFileLocation+UploadFiles[i],mappingFile,wsURL, completeDir,false);
		        }catch(java.lang.SecurityException se){
		        	
		        }
		        moveFiles(new String[]{CompletedFileLocation + UploadFiles[i]}, "/home/bloomnet/public/ftpFiles/SalesForceExport/", currYear);
		        logger.debug("Upload to CRM On Demand COMPLETE for file: "+inputFileName);
	  			
		    }
			
			//BulkOpsClientShutdownHandler.getInstance().signalReadyToExit();
			

			//errWriter.close();
			System.out.println("DONE!");
			logger.debug("All the files have been processed succesfully");
			//System.setSecurityManager(sm.getOriginalSecurityManager() );
			System.exit(0);

		}
		catch(Exception e)
		{
			e.printStackTrace();
			logger.error("Error in main: "+e.getMessage());
		}
		

	}
	
	protected void moveFiles(String [] filesToMove, String completeDir, boolean currYear){
		
		for(String f : filesToMove){
			
			File completedFileName = new File(f);
			boolean isMoved = false;
			if(currYear)	
				isMoved=completedFileName.renameTo(new File(completeDir,"mtdcurrent.csv"));
			else
				isMoved=completedFileName.renameTo(new File(completeDir,"mtdprior.csv"));
	        if (isMoved) {
	            logger.debug(" The file "+ completedFileName.getName() + " was processed and moved to completed folder");
	        }
	        else{
	        	logger.error(" Failed to move the processsed file: "+ completedFileName.getAbsolutePath());
	        }
		}
	}
	
	




	

}
