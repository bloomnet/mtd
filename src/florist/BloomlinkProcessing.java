package florist;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;

import org.apache.log4j.PropertyConfigurator;

import bulkOperations.BulkOpsClientShutdownHandler;


public class BloomlinkProcessing extends ImportFloristData{
	File newFile = null;
	FileWriter writer = null;
	File newFile2 = null;
	FileWriter writer2 = null;
	
	public String createCSVFromBlkFile(String fullPath, String file, String completedFileLocation){
		try{	
			BufferedReader br = new BufferedReader(new FileReader(fullPath));
  			String nextRow;
  			newFile = new File(completedFileLocation + file.replaceAll(".txt", "") +".csv");
  			writer = new FileWriter(newFile);
  			newFile2 = new File("/home/bloomnet/public/ftpFiles/SalesForceExport/bloomlink.csv");
  			writer2 = new FileWriter(newFile2);
  			
  			//Read each row from the input file and insert it to CRM
  			while((nextRow = br.readLine())!= null)
  			{
  				String[] strAryRow = nextRow.split("\t");
    			
    			try
    			{
    				String shopCode = strAryRow[9];
    				String sunday = strAryRow[13];
	    			String dontsend = strAryRow[15];
	    			String displayName = strAryRow[20];
	    			String legalName = strAryRow[21];
	    			if(shopCode != null && shopCode.trim().length() > 5 && sunday != null && sunday.length() > 0 && dontsend != null && dontsend.length() > 0){
	    				if(dontsend.equals("Y"))
	    					dontsend = "N";
	    				else if(dontsend.equals("N"))
	    					dontsend = "Y";
	    				writer.append("\""+shopCode+"\",\""+sunday+"\",\""+dontsend+"\",\""+displayName+"\",\""+legalName+"\"\n");
	    				writer2.append("\""+nextRow.replaceAll("\t","\",\"")+"\"\n");
	    			}

	    			
    			}
    			catch(Exception ex)
    			{
    				logger.error(ex.getMessage());
    				ex.printStackTrace();
    			}
    		} 
  			
  			br.close();
  			writer.flush();
  			writer.close();
  			writer2.flush();
  			writer2.close();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			logger.error("Error in main: "+e.getMessage());
		}
		
		return completedFileLocation + newFile.getName();
		
	}
	
	
	@Override
	public void process() {
		
		
		//ExitManager sm;
		
		try{
			//Configure Log details
			PropertyConfigurator.configure("log4j_blk.properties");
			logger.debug("Process Started...");
			
			//Reading properties file
			HashMap loadProperties = ReadProperties.ReadPropertiesFile("BloomnetFloristBLK.properties");
			
			System.out.println(loadProperties);
			System.out.println(loadProperties.get("blk.UploadFileLocation"));
			System.out.println(loadProperties.get("CRMODPassword"));
			
			String UploadFileLocation=loadProperties.get("blk.UploadFileLocation").toString();
			String ErrorFileLocation=loadProperties.get("blk.ErrorFileLocation").toString();
			String CompletedFileLocation=loadProperties.get("blk.CompletedFileLocation").toString();
			String mappingFileBLK = loadProperties.get("MappingFilesLocation").toString()+loadProperties.get("blk.mappingfile").toString();
			
			loadProperties.get("CRMODUrl").toString();
			loadProperties.get("CRMODUsername").toString();
			loadProperties.get("CRMODPassword").toString();
			
			useProxy = loadProperties.get("UseProxy").toString();
			proxyHost = loadProperties.get("ProxyHost").toString();
			proxyPort = loadProperties.get("ProxyPort").toString();
			proxyUsername = loadProperties.get("ProxyUser").toString();
			proxyPassword = loadProperties.get("ProxyPassword").toString();

			
			File uploadDir = new File(UploadFileLocation);
			File errorDir = new File(ErrorFileLocation);
			File completeDir = new File(CompletedFileLocation);
			
			//Checking input file folder
  			if(!uploadDir.exists()){
				throw new Exception("Upload folder does not exist: "+UploadFileLocation);	
			}
  			//Checking input file existance
  			String[] UploadFiles = uploadDir.list();
    		if(UploadFiles.length <= 0)
    		{
    			throw new FileNotFoundException();
    		}
    		logger.debug("Number of input files required processing: " +UploadFiles.length);
    		//Checking for Completed file folder 
			if(!completeDir.exists()){
				completeDir.mkdir();	
			}
			//Checking for Error file folder 
			if(!errorDir.exists()){
				errorDir.mkdir();
			}				
		    //sm = new ExitManager( System.getSecurityManager() );
		     //System.setSecurityManager(sm);
			
			for (int i=0; i<UploadFiles.length; i++) 
			{   
				
				String mappingFile = mappingFileBLK;
		        String inputFileName= UploadFileLocation + UploadFiles[i];
		        String ext="";
		        int mid= inputFileName.lastIndexOf(".");
		        ext=inputFileName.substring(mid+1,inputFileName.length()); 

		        if(!("TXT".equalsIgnoreCase(ext))){
		        	continue;
		        }

		        logger.debug("Processing input file name: " + inputFileName);
		        String csvFileName = createCSV(inputFileName);//asuft 08072013
		        String [] filesToMove = {inputFileName};
		        moveFiles(filesToMove, CompletedFileLocation);
		        logger.debug("Going to upload data to CRM On Demand....");
		        try{
		        	logger.debug("Going upload CSV file: "+csvFileName+" with mapping: "+mappingFile);
		        	//sendData("Account","update",user,userPwd,csvFileName,mappingFile,wsURL, completeDir,false);
		        }catch(java.lang.SecurityException se){
		        	
		        }
		        logger.debug("Upload to CRM On Demand COMPLETE for file: "+inputFileName);
	  			
		        break;
		    }
			
			BulkOpsClientShutdownHandler.getInstance().signalReadyToExit();
			

			//errWriter.close();
			System.out.println("DONE!");
			logger.debug("All the files have been processed succesfully");
			//System.setSecurityManager(sm.getOriginalSecurityManager() );
			System.exit(0);

		}
		catch(Exception e)
		{
			e.printStackTrace();
			logger.error("Error in main: "+e.getMessage());
		}
		

	}
	
	public String createCSV (String fullPath){
		File newFile = null;
		try{	
			BufferedReader br = new BufferedReader(new FileReader(fullPath));
  			String nextRow;
  			newFile = new File("/home/bloomnet/public/ftpFiles/SalesForceExport/bloomlink.csv");
  			FileWriter writer = new FileWriter(newFile);
  			 
  			while((nextRow = br.readLine())!= null)
  			{    			
    			try{
    				String line = "\""+nextRow.replaceAll("\"", "").replaceAll("\t","\",\"")+"\"";
    				String[] lineItems = line.split("\",\"");
    				if(lineItems.length < 33){
    					int diff = 33 - (lineItems.length);
    					writer.append(line);
    					for(int ii=0; ii<diff; ++ii){
    						writer.append(",\"\"");
    					}
    					writer.append("\r\n");
    				}else if(lineItems.length > 33){
    					//Do Nothing!!!
    				}
    				else{
    					writer.append(line+"\r\n");
    				}
    				
    			}catch(Exception ex){
    				logger.error(ex.getMessage());
    				ex.printStackTrace();
    			}
    		} 
  			
  			br.close();
  			writer.flush();
  			writer.close();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			logger.error("Error in main: "+e.getMessage());
		}
		
		return "/home/bloomnet/public/ftpFiles/SalesForceExport/" + newFile.getName();
		
	}
	
	
}
