package florist;

public class CheckLastPage {
	
	String lastPage="";
	
	public CheckLastPage(String check)
	{
		setLastPage(check);
	}

	public boolean getLastPage() {
		if(lastPage.equalsIgnoreCase("True"))
		{
			return true;	
		}
		else{
			return false;
		}
	}

	public void setLastPage(String LastPage) {
		this.lastPage = LastPage;
	}

}
