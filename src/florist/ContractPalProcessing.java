package florist;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.PropertyConfigurator;

import bulkOperations.BulkOpsClientShutdownHandler;


public class ContractPalProcessing extends ImportFloristData{
	File newFile = null;
	FileWriter writer = null;
	File newFile2 = null;
	FileWriter writer2 = null;
	
	public List<String> createCSVFromCPFile(String fullPath, String file, String completedFileLocation){
		List<String> returnInfo = new ArrayList<String>();
		try{	
			BufferedReader br = new BufferedReader(new FileReader(fullPath));
  			String nextRow;
  			newFile = new File(completedFileLocation + file.replace(".csv", "Shop.csv"));
  			writer = new FileWriter(newFile);
  			newFile2 = new File(completedFileLocation + file.replace(".csv", "Lifecycle.csv"));
  			writer2 = new FileWriter(newFile2);
  			
  			nextRow = br.readLine();//skip first line, it's just headers.
  			if((nextRow = br.readLine())!= null)
  			{
  				String[] strAryRow = nextRow.split("\",\"");
    			
    			try
    			{
    				String shopName = strAryRow[148];
    				String address = strAryRow[137];
    				String city = strAryRow[127];
    				String state = strAryRow[130];
    				String zip = strAryRow[132];
    				String country = strAryRow[150].replaceAll(".", "");
    				String existingRowId = strAryRow[25];
    				String sunday = strAryRow[24];
    				String contact = strAryRow[71];
    				String phone = strAryRow[147];
    				String fax = strAryRow[140];
    				String email = strAryRow[129];
    				String promotion = strAryRow[97];
    				String mac = strAryRow[21];
    				String installType = strAryRow[46];
    				String keepExistingShopCode = strAryRow[43];
    				String existingShopCode = strAryRow[39];
    				String shopCode = "";
    				String billingDate = "";
    				boolean isUpdate = false;
    				boolean isExisting = false;
    				
    				if(sunday != null && ! sunday.trim().equals("")){
    					if (sunday.equalsIgnoreCase("TRUE")){
    						sunday = "N";
    					}else{
    						sunday = "Y";
    					}
    				}else{
    					sunday = "N";
    				}
    				
    				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    				billingDate = sdf.format(new Date());
    				
    				if(installType != null && !installType.equalsIgnoreCase("Change of Ownership")){
    					installType = "Standard";
    				}
    				
    				if(mac != null){
    					String[] macParts = mac.split(" ");
    					String firstName = macParts[0];
    					String lastName = macParts[1];
    					mac ="BLOOMNET/" + firstName.substring(0, 1).toUpperCase() + lastName.toUpperCase();
    				}
    				
    				writer2.append("shopCode,installType,promotion,billingDate,contractType,floristCode,mac\n");
    				
    				if(existingRowId != null && !existingRowId.equals("")){
    					isUpdate = true;
						shopCode = getShopCode();
						writer.append("rowId,shopName,address,city,state,zip,country,openSunday,contact,phone,fax,email,shopcode,mac,status\n");
    					writer.append(existingRowId+","+shopName+","+address+","+city+","+state+","+zip+","+country+","+sunday+","+contact+","+phone+","+fax+","+email+","+shopCode+","+mac+",Active\n");
    					writer2.append(shopCode+","+installType+","+promotion+","+billingDate+",Contract Pal Application,"+shopCode+","+mac+"\n");
					
    				}else if(keepExistingShopCode != null && keepExistingShopCode.equalsIgnoreCase("Y")){
						writer.append("shopCode,shopName,address,city,state,zip,country,openSunday,contact,phone,fax,email,mac,status\n");
    					writer.append(existingShopCode+","+shopName+","+address+","+city+","+state+","+zip+","+country+","+sunday+","+contact+","+phone+","+fax+","+email+","+mac+",Active\n");
    					writer2.append(existingShopCode+","+installType+","+promotion+","+billingDate+",Contract Pal Application,"+existingShopCode+","+mac+"\n");
    					isExisting = true;
    					
    				}else{
    					shopCode = getShopCode();
    					writer.append("shopCode,shopName,address,city,state,zip,country,openSunday,contact,phone,fax,email,mac,status\n");
    					writer.append(shopCode+","+shopName+","+address+","+city+","+state+","+zip+","+country+","+sunday+","+contact+","+phone+","+fax+","+email+","+mac+",Active\n");
    					writer2.append(shopCode+","+installType+","+promotion+","+billingDate+",Contract Pal Application,"+shopCode+","+mac+"\n");
    				}   				
    				
    				writer.flush();
    				writer2.flush();
    				returnInfo.add(completedFileLocation + newFile.getName());
    				returnInfo.add(completedFileLocation + newFile2.getName());
    				returnInfo.add(String.valueOf(isUpdate));
    				returnInfo.add(String.valueOf(isExisting));


	    			
    			}
    			catch(Exception ex)
    			{
    				logger.error(ex.getMessage());
    				ex.printStackTrace();
    			}
    		} 
  			
  			br.close();
  			writer.flush();
  			writer2.flush();
  			writer.close();
  			writer2.close();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			logger.error("Error in main: "+e.getMessage());
		}
		return returnInfo;
		
	}
	
	private String getShopCode(){
		String shopCode = "";
		File file = new File("/root/shopCodes.csv");
		
		  try //vacation leave/
		  {
		      File tempFile = new File("/root/shopCodesTemp.csv");

		      BufferedReader reader = new BufferedReader(new FileReader(file));
		      BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

		      String currentLine;

		      if((currentLine = reader.readLine()) != null)
		      {
		          shopCode = currentLine.trim();
		      }   
		      
		      while((currentLine = reader.readLine()) != null){
		    	  writer.write(currentLine+"\n"); 
		      }
		      writer.close();
		      reader.close();

		      file.delete();
		      tempFile.renameTo(file);

		  }
		  catch(Exception e)
		  {
		      e.printStackTrace();
		  }
		
		return shopCode;
	}
	
	
	@SuppressWarnings({ "rawtypes", "unused" })
	@Override
	public void process() {
	
		ExitManager sm;
		
		try{
			//Configure Log details
			PropertyConfigurator.configure("log4j_cp.properties");
			logger.debug("Process Started...");
			
			//Reading properties file
			HashMap loadProperties = ReadProperties.ReadPropertiesFile("/UploadCRMData/BloomnetFloristCP.properties");
			
			String UploadFileLocation=loadProperties.get("cp.UploadFileLocation").toString();
			String ErrorFileLocation=loadProperties.get("cp.ErrorFileLocation").toString();
			String CompletedFileLocation=loadProperties.get("cp.CompletedFileLocation").toString();
			String mappingFileCPNewShop = loadProperties.get("MappingFilesLocation").toString()+loadProperties.get("cp.mappingfilenewshop").toString();
			String mappingFileCPUpdateShop = loadProperties.get("MappingFilesLocation").toString()+loadProperties.get("cp.mappingfileupdateshop").toString();
			String mappingFileCPLifecycle = loadProperties.get("MappingFilesLocation").toString()+loadProperties.get("cp.mappingfilelifecycle").toString();
			
			// //CRMODUsername, CRMODPassword, CRMODUrl
			String wsURL = loadProperties.get("CRMODUrl").toString();
			String user = loadProperties.get("CRMODUsername").toString();
			String userPwd = loadProperties.get("CRMODPassword").toString();
			
			useProxy = loadProperties.get("UseProxy").toString();
			proxyHost = loadProperties.get("ProxyHost").toString();
			proxyPort = loadProperties.get("ProxyPort").toString();
			proxyUsername = loadProperties.get("ProxyUser").toString();
			proxyPassword = loadProperties.get("ProxyPassword").toString();

			
			File uploadDir = new File(UploadFileLocation);
			File errorDir = new File(ErrorFileLocation);
			File completeDir = new File(CompletedFileLocation);
			
			//Checking input file folder
  			if(!uploadDir.exists()){
				throw new Exception("Upload folder does not exist: "+UploadFileLocation);	
			}
  			//Checking input file existance
  			String[] uploadFiles = uploadDir.list();
    		if(uploadFiles.length <= 0)
    		{
    			throw new FileNotFoundException();
    		}
    		logger.debug("Number of input files required processing: " +uploadFiles.length);
    		//Checking for Completed file folder 
			if(!completeDir.exists()){
				completeDir.mkdir();	
			}
			//Checking for Error file folder 
			if(!errorDir.exists()){
				errorDir.mkdir();
			}				
		    sm = new ExitManager( System.getSecurityManager() );
		     System.setSecurityManager(sm);
			
			for (int ii=0; ii<uploadFiles.length; ++ii) 
			{   
				String inputFileName= UploadFileLocation + uploadFiles[ii];
				
				logger.debug("Processing input file name: " + inputFileName);
				
				List<String> processingInfo = createCSVFromCPFile(inputFileName,uploadFiles[ii], CompletedFileLocation);
				String doUpdate = processingInfo.get(2);
				String doExisting = processingInfo.get(3);
				String mappingFileShop = "";
				if(doUpdate.equalsIgnoreCase("True")){
					mappingFileShop = mappingFileCPUpdateShop;
				}else{
					mappingFileShop = mappingFileCPNewShop;
				}

		        String csvFileNameShop = processingInfo.get(0);
		        String csvFileNameLifecycle =  processingInfo.get(1);
		        String [] filesToMove = {inputFileName};
		        moveFiles(filesToMove, CompletedFileLocation);
		        logger.debug("Going to upload data to CRM On Demand....");
		        try{
		        	logger.debug("Going upload CSV file: "+csvFileNameShop+" with mapping: "+mappingFileShop);
		        	if(doUpdate.equalsIgnoreCase("True")){
			        	sendData("Account","update",user,userPwd,csvFileNameShop,mappingFileShop,wsURL, completeDir,true);
		        	}else if(doExisting.equalsIgnoreCase("True")){
			        	sendData("Account","update",user,userPwd,csvFileNameShop,mappingFileShop,wsURL, completeDir,false);
		        	}else{
			        	sendData("Account","insert",user,userPwd,csvFileNameShop,mappingFileShop,wsURL, completeDir,false);
		        	}
		        }catch(java.lang.SecurityException se){
		        	
		        }
		        try{
		        	System.out.println("Going upload CSV file: "+csvFileNameLifecycle+" with mapping: "+mappingFileCPLifecycle);
		        	sendData("CustomObject02","insert",user,userPwd,csvFileNameLifecycle,mappingFileCPLifecycle,wsURL, completeDir,false);
		        }catch(Exception ee){
		        	
		        }
		        logger.debug("Upload to CRM On Demand COMPLETE for file: "+inputFileName);
	  		
		    }
			
			System.out.println("DONE!");
			logger.debug("All the files have been processed succesfully");
			System.setSecurityManager(sm.getOriginalSecurityManager() );
			BulkOpsClientShutdownHandler.getInstance().signalReadyToExit();
			System.exit(0);

		}
		catch(Exception e)
		{
			e.printStackTrace();
			logger.error("Error in main: "+e.getMessage());
		}
		

	}
	
	
}
