/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  bulkOperations.BulkImportRequestQueue
 *  bulkOperations.BulkImportRequestQueueElement
 *  bulkOperations.BulkOpsClientLocalizationKit
 *  bulkOperations.BulkOpsClientOptionManager
 *  bulkOperations.BulkOpsClientShutdownHandler
 *  bulkOperations.BulkOpsClientUtil
 *  bulkOperations.CSVDataSender
 *  bulkOperations.CSVManager
 *  bulkOperations.FieldMappingManager
 *  bulkOperations.ManifestManager
 *  bulkOperations.ODWSSessionKeeperThread
 *  bulkOperations.SOAPImpRequestManager
 *  javax.xml.rpc.ServiceException
 */
package florist;

import bulkOperations.BulkImportRequestQueue;
import bulkOperations.BulkImportRequestQueueElement;
import bulkOperations.BulkOpsClientLocalizationKit;
import bulkOperations.BulkOpsClientLogger;
import bulkOperations.BulkOpsClientOptionManager;
import bulkOperations.BulkOpsClientShutdownHandler;
import bulkOperations.BulkOpsClientUtil;
import bulkOperations.CSVDataSender;
import bulkOperations.CSVManager;
import bulkOperations.FieldMappingManager;
import bulkOperations.ManifestManager;
import bulkOperations.ODWSSessionKeeperThread;
import bulkOperations.ODWSSessionManager;
import bulkOperations.SOAPImpRequestManager;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Locale;
import javax.xml.rpc.ServiceException;

public final class BulkOpsClient {
    private static final int FATAL_ERROR = -2;
    private static final int ERROR = -1;
    private static final int OK = 0;
    private static final String CONFIG_FILE_NAME = ".\\config\\OracleDataLoaderOnDemand.config";
    private static BulkOpsClientOptionManager m_optionManager;
    private static BulkOpsClientLogger m_logger;
    private static BulkOpsClientLocalizationKit m_localizationKit;
    private static ODWSSessionManager m_mgrSession;
    private static BulkImportRequestQueue m_bulkImportReqQueue;
    private static ManifestManager m_mgrManifest;
    private static CSVDataSender m_sndrCSVData;
    private static BulkOpsClientShutdownHandler m_shutdownHdlr;
    private static String m_strRequestId;
    private static Thread m_dataLoaderSessionKeeper;
    private static boolean m_bWaitForCompletionFlag;

    public static void main(String[] arrstring) {
        block25 : {
            Object[] arrobject = new String[1];
            Object[] arrobject2 = new String[2];
            try {
                String string;
                try {
                    m_localizationKit = BulkOpsClientLocalizationKit.getInstance();
                    BulkOpsClientLocalizationKit.setLocale((String)"ENU");
                }
                catch (Exception var5_3) {
                    System.err.print("An error occured while initializing client:");
                    var5_3.printStackTrace();
                    System.exit(1);
                }
                m_shutdownHdlr = BulkOpsClientShutdownHandler.getInstance();
                m_shutdownHdlr.registerShutdownHook();
                m_optionManager = BulkOpsClientOptionManager.getInstance();
                try {
                    m_optionManager.parseAndValidateConfig(".\\config\\OracleDataLoaderOnDemand.config");
                }
                catch (Exception var5_4) {
                    arrobject[0] = var5_4.getMessage();
                    string = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_CLIENT_CONFIG_LOAD_ERR", (Object[])arrobject);
                    System.err.println(string);
                    BulkOpsClient.finalize_BulkOpsClient(1);
                }
                try {
                    m_optionManager.parseAndValidateOption(arrstring);
                }
                catch (Exception var5_6) {
                    if (BulkOpsClientOptionManager.isHelpWanted() || arrstring.length == 0) {
                        System.out.println(BulkOpsClientOptionManager.getHelpInfo());
                        m_bWaitForCompletionFlag = false;
                        BulkOpsClient.finalize_BulkOpsClient(0);
                    }
                    arrobject[0] = var5_6.getMessage();
                    string = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_CLIENT_INPUT_PARSE_ERR", (Object[])arrobject);
                    System.err.println(string);
                    BulkOpsClient.finalize_BulkOpsClient(1);
                }
                if (BulkOpsClientOptionManager.isHelpWanted()) {
                    System.out.println(BulkOpsClientOptionManager.getHelpInfo());
                    m_bWaitForCompletionFlag = false;
                    BulkOpsClient.finalize_BulkOpsClient(0);
                }
                m_bWaitForCompletionFlag = BulkOpsClientOptionManager.getOptionValueBool((String)"waitforcompletion");
                String string2 = BulkOpsClientOptionManager.getOptionValueStr((String)"clientlogfiledir");
                String string3 = BulkOpsClientOptionManager.getOptionValueStr((String)"clientloglevel");
                m_logger = BulkOpsClientLogger.getInstance();
                m_logger.init(string2, string3);
                if (m_logger.isInitialized()) {
                    m_logger.debug("BulkOpsClient.main(): Execution begin.");
                    m_logger.debug("BulkOpsClient.main(): List of all configurations loaded: " + BulkOpsClientOptionManager.getLoadedConfigsAsString());
                    m_logger.debug("BulkOpsClient.main(): List of all options loaded: " + BulkOpsClientOptionManager.getLoadedOptionsAsString());
                    String string4 = BulkOpsClientOptionManager.getOptionValueStr((String)"username");
                    String string5 = BulkOpsClientOptionManager.getOptionValueStr((String)"operation");
                    String string6 = BulkOpsClientOptionManager.getConfigValueStr((String)"routingurl");
                    int n = BulkOpsClientOptionManager.getConfigValueInt((String)"logintimeoutms");
                    String string7 = BulkOpsClientOptionManager.getConfigValueStr((String)"hosturl");
                    String string8 = BulkOpsClientOptionManager.getConfigValueStr((String)"testmode");
                    String string9 = BulkOpsClientOptionManager.getOptionValueStr((String)"hosturl");
                    String string10 = BulkOpsClientOptionManager.getOptionValueStr((String)"proxyserver");
                    String string11 = BulkOpsClientOptionManager.getOptionValueStr((String)"proxyserverport");
                    String string12 = BulkOpsClientOptionManager.getOptionValueStr((String)"proxyauthusername");
                    int n2 = -1;
                    if (string10 != null && string10.length() > 0) {
                        if (string11 == null || string11.length() == 0) {
                            arrobject2[0] = "proxyserverport";
                            arrobject2[1] = "proxyserver";
                            String string13 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_CLIENT_PARENT_CHILD_MISS_PROP_ERR", (Object[])arrobject2);
                            System.err.println(string13);
                            BulkOpsClient.finalize_BulkOpsClient(1);
                        } else {
                            try {
                                n2 = Integer.parseInt(string11);
                                System.setProperty("http.proxyHost", string10);
                                System.setProperty("http.proxyPort", string11);
                                System.setProperty("https.proxyHost", string10);
                                System.setProperty("https.proxyPort", string11);
                            }
                            catch (Exception var18_25) {
                                String string14 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_CSV_INVALID_ARGS");
                                System.err.println(string14);
                                BulkOpsClient.finalize_BulkOpsClient(1);
                            }
                        }
                    }
                    String string15 = BulkOpsClientUtil.getPassword((String)BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_CLIENT_PWD_PROMPT"));
                    String string16 = null;
                    if (string12 != null && string12.length() > 0 && string10 != null && string10.length() > 0 && n2 > 0) {
                        string16 = BulkOpsClientUtil.getProxyUserPassword((String)BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_CLIENT_PROXY_USERPWD_PROMPT"));
                    }
                    if (string9 == null || string9.length() == 0) {
                        if (!string8.equals("debug")) {
                            String string17 = BulkOpsClientUtil.lookupHostURL((String)string6, (String)string4, (int)n);
                            string7 = BulkOpsClientUtil.determineWSHostURL((String)string17, (String)string7, (String)".\\config\\OracleDataLoaderOnDemand.config");
                        }
                    } else {
                        string7 = string9;
                    }
                    int n3 = BulkOpsClientOptionManager.getConfigValueInt((String)"maxloginattempts");
                    m_mgrSession = new ODWSSessionManager(string7, n);
                    string = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_SESS_LOGIN_ATTEMPT");
                    System.out.println(string);
                    m_logger.info(string);
                    if (m_mgrSession.login(string4, string15, string10, n2, string12, string16, n3)) {
                        arrobject[0] = string4;
                        string = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_SESS_LOGIN_SUCCESS", (Object[])arrobject);
                        System.out.println(string);
                        m_logger.info(string);
                        int n4 = BulkOpsClientOptionManager.getConfigValueInt((String)"sessionkeepchkinterval");
                        m_dataLoaderSessionKeeper = new Thread((Runnable)new ODWSSessionKeeperThread(m_mgrSession.getURLSessionId(), n4, string12, string16));
                        m_dataLoaderSessionKeeper.start();
                        if (string5.equals("insert") || string5.equals("update") || string5.equals("resume")) {
                            int n5 = BulkOpsClient.doImport(string5, string12, string16);
                            if (m_bWaitForCompletionFlag) {
                                int n6 = BulkOpsClientOptionManager.getConfigValueInt((String)"impstatchkinterval");
                                BulkOpsClient.waitForImportCompletion(m_mgrSession.getURLSessionId(), m_strRequestId, n6, n5 != 0, string12, string16);
                                m_bWaitForCompletionFlag = false;
                            }
                        }
                        string = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_SESS_LOGOUT_ATTEMPT");
                        System.out.println(string);
                        m_logger.info(string);
                        m_mgrSession.logout(string10, n2, string12, string16);
                        arrobject[0] = string4;
                        string = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_SESS_LOGOUT_SUCCESS", (Object[])arrobject);
                        System.out.println(string);
                        m_logger.info(string);
                    } else {
                        string = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_SESS_AUTH_ERR");
                        System.err.println(string);
                        BulkOpsClient.finalize_BulkOpsClient(1);
                    }
                    break block25;
                }
                string = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_CLIENT_LOG_INIT_FAIL");
                System.out.println(string);
                BulkOpsClient.finalize_BulkOpsClient(1);
            }
            catch (Throwable var5_7) {
                Object[] arrobject3 = new String[]{BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_OD_CC")};
                String string = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_UNEXPECTED_ERR", (Object[])arrobject3);
                if (m_logger != null && m_logger.isInitialized()) {
                    m_logger.error(string, var5_7);
                }
                System.err.println(string);
                BulkOpsClient.finalize_BulkOpsClient(1);
            }
        }
        BulkOpsClient.finalize_BulkOpsClient(0);
    }

    protected static void finalize_BulkOpsClient(int n) {
        Object[] arrobject = new String[1];
        if (m_bWaitForCompletionFlag) {
            BulkOpsClient.waitForImportCompletion(null, null, 0, true, null, null);
        }
        if (m_dataLoaderSessionKeeper != null && m_dataLoaderSessionKeeper.isAlive()) {
            m_dataLoaderSessionKeeper.interrupt();
            try {
                m_dataLoaderSessionKeeper.join();
            }
            catch (InterruptedException var2_2) {
                m_logger.debug("BulkOpsClient.finalize_BulkOpsClient(): Interrupted.");
            }
        }
        if (m_logger != null && m_logger.isInitialized()) {
            arrobject[0] = m_logger.getLogFilePath();
            String string = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_LOGFILE", (Object[])arrobject);
            System.out.println(string);
            m_logger.debug("BulkOpsClient.main(): Execution complete.");
        }
    }

    private static int doImport(String string, String string2, String string3) {
        int n = 0;
        String string4 = "";
        String string5 = "";
        String string6 = "";
        String string7 = "";
        String string8 = "";
        String string9 = m_mgrSession.getLoginId();
        boolean bl = true;
        Object[] arrobject = new String[1];
        HashMap<String, String> hashMap = new HashMap<String, String>();
        m_logger.debug("BulkOpsClient.doImport(): Execution begin.");
        if (!string.equals("resume")) {
            int n2;
            int n3;
            Object object;
            string5 = BulkOpsClientOptionManager.getOptionValueStr((String)"datafilepath");
            string6 = BulkOpsClientOptionManager.getOptionValueStr((String)"csvdelimiter");
            string7 = BulkOpsClientOptionManager.getOptionValueStr((String)"mapfilepath");
            string8 = BulkOpsClientOptionManager.getOptionValueStr((String)"recordtype");
            bl = !BulkOpsClientOptionManager.getOptionValueBool((String)"disableimportaudit");
            string8 = BulkOpsClient.convertFirstLetterToUpperCases(string8);
            hashMap.put("CSVDelimiter", string6);
            hashMap.put("DateTimeFormat", BulkOpsClientOptionManager.getOptionValueStr((String)"datetimeformat"));
            hashMap.put("DuplicateCheckOption", BulkOpsClientOptionManager.getOptionValueStr((String)"duplicatecheckoption"));
            hashMap.put("ErrorLogLevel", BulkOpsClientOptionManager.getOptionValueStr((String)"importloglevel"));
            String string10 = string5.substring(string5.lastIndexOf("\\") + 1);
            try {
                File file = new File(string5);
                string5 = file.getCanonicalPath();
            }
            catch (IOException var14_16) {
                arrobject[0] = string5;
                string4 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_CSV_ACCESS_ERR", (Object[])arrobject);
                System.err.println(string4);
                m_logger.error(string4);
                return -1;
            }
            try {
                File file = new File(string7);
                string7 = file.getCanonicalPath();
            }
            catch (IOException var14_17) {
                arrobject[0] = string7;
                string4 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_CSV_ACCESS_ERR", (Object[])arrobject);
                System.err.println(string4);
                m_logger.error(string4);
                return -1;
            }
            n = BulkOpsClient.sendValidationRequest(string8, string7, string5, string10, string6, hashMap, string, Boolean.toString(bl), string2, string3);
            if (n == 0 && !(BulkOpsClient.m_mgrManifest = ManifestManager.getInstance()).initManifest(string9, (String)(object = BulkOpsClientOptionManager.getConfigValueStr((String)"manifestfiledir")), m_strRequestId, string5, string6, n3 = BulkOpsClientOptionManager.getConfigValueInt((String)"maxsoapsize"), n2 = BulkOpsClientOptionManager.getConfigValueInt((String)"csvblocksize"))) {
                n = -1;
            }
        } else {
            m_strRequestId = BulkOpsClientOptionManager.getOptionValueStr((String)"resumerequest");
            String string11 = BulkOpsClientOptionManager.getConfigValueStr((String)"manifestfiledir");
            m_mgrManifest = ManifestManager.getInstance();
            if (!m_mgrManifest.loadManifest(string11, m_strRequestId)) {
                arrobject[0] = m_strRequestId;
                string4 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_UNABLE_TO_RESUME", (Object[])arrobject);
                m_logger.error(string4);
                System.err.println(string4);
                string4 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_RECHECK_RESUME_REQ_ID");
                m_logger.error(string4);
                System.err.println(string4);
                string4 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_RETRY_RESUME");
                m_logger.error(string4);
                System.err.println(string4);
                n = -1;
            } else if (m_mgrManifest.getLoginIdFromManifest().compareTo(string9) != 0) {
                arrobject[0] = m_mgrManifest.getLoginIdFromManifest();
                string4 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_RESUME_USER_ID_NO_MATCH", (Object[])arrobject);
                System.err.println(string4);
                m_logger.error(string4);
                n = -1;
            } else {
                string5 = m_mgrManifest.getCSVDataFilePath();
                string6 = m_mgrManifest.getCSVDelimiter();
            }
        }
        if (!m_shutdownHdlr.isShutdown() && n == 0) {
            n = BulkOpsClient.submitImportRequest(string5, string6, string2, string3);
        }
        m_logger.debug("BulkOpsClient.doImport(): Execution complete.");
        return n;
    }

    private static int submitImportRequest(String string, String string2, String string3, String string4) {
        int n;
        int n2 = 0;
        int n3 = 2;
        String string5 = "";
        Object[] arrobject = new String[1];
        Object[] arrobject2 = new String[1];
        m_logger.debug("BulkOpsClient.submitImportRequest(): Execution begin.");
        m_bulkImportReqQueue = BulkImportRequestQueue.getInstance();
        Object[] arrobject3 = m_mgrManifest.getDataSegmentIDsAsArray();
        for (n = arrobject3.length - 1; n >= 0; --n) {
            String string6 = m_mgrManifest.getSegmentStatusById(((Integer)arrobject3[n]).intValue());
            if (!string6.equals("Queued")) continue;
            m_bulkImportReqQueue.put(new BulkImportRequestQueueElement(((Integer)arrobject3[n]).intValue()));
        }
        n = BulkOpsClientOptionManager.getConfigValueInt((String)"numofthreads");
        int n4 = BulkOpsClientOptionManager.getConfigValueInt((String)"maxthreadfailure");
        m_logger.debug("BulkOpsClient.submitImportRequest(): Sending CSV Data Segments.");
        m_sndrCSVData = new CSVDataSender(m_mgrSession.getURLSessionId(), m_strRequestId, string, string2, n, n4, string3, string4);
        arrobject[0] = m_strRequestId;
        string5 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_IMPORT_SUBMITTING", (Object[])arrobject);
        System.out.println(string5);
        m_logger.info(string5);
        n3 = m_sndrCSVData.sendCSVData();
        if (n3 == 0) {
            string5 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_IMPORT_SUBMISSION_COMPLETE");
            System.out.println(string5);
            m_logger.info(string5);
            n2 = 0;
        } else if (n3 == 1) {
            string5 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_IMPORT_SUBMISSION_PARTIAL_COMPLETE");
            System.err.println(string5);
            m_logger.error(string5);
            arrobject[0] = string;
            arrobject2[0] = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_OD_TECH_SUPPORT");
            string5 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_IMPORT_RETRY", (Object[])arrobject2);
            m_logger.error(string5);
            System.err.println(string5);
            n2 = -1;
            if (m_strRequestId != null) {
                arrobject[0] = m_strRequestId;
                string5 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_RESUME_COMMAND_WITH_REQ_ID", (Object[])arrobject);
                m_logger.error(string5);
                System.err.println(string5);
            }
        } else if (n3 == 2) {
            string5 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_IMPORT_SUBMISSION_FAILED");
            System.err.println(string5);
            m_logger.error(string5);
            arrobject[0] = string;
            arrobject2[0] = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_OD_TECH_SUPPORT");
            string5 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_IMPORT_RETRY", (Object[])arrobject2);
            m_logger.error(string5);
            System.err.println(string5);
            n2 = -1;
            if (m_strRequestId != null) {
                arrobject[0] = m_strRequestId;
                string5 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_RESUME_COMMAND_WITH_REQ_ID", (Object[])arrobject);
                m_logger.error(string5);
                System.err.println(string5);
            }
        } else if (n3 == 3) {
            string5 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_IMPORT_SUBMISSION_CANCELLED");
            System.out.println(string5);
            m_logger.info(string5);
            n2 = -1;
        }
        m_mgrManifest.finalizeManifest();
        m_logger.debug("BulkOpsClient.submitImportRequest(): Execution complete.");
        return n2;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static int sendValidationRequest(String string, String string2, String string3, String string4, String string5, HashMap hashMap, String string6, String string7, String string8, String string9) {
        int n;
        block18 : {
            n = 0;
            Object[] arrobject = new String[1];
            String string10 = "";
            String string11 = "";
            String string12 = "";
            HashMap hashMap2 = new HashMap();
            int n2 = 0;
            String string13 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_IMPORT_VALIDATING");
            System.out.println(string13);
            m_logger.info(string13);
            FieldMappingManager fieldMappingManager = new FieldMappingManager(string2);
            if (m_shutdownHdlr.isShutdown()) {
                n = -1;
            } else {
                try {
                    hashMap2 = fieldMappingManager.parseMappings();
                    CSVManager cSVManager = new CSVManager();
                    if (!cSVManager.init(string3, string5)) {
                        n = -1;
                    }
                    string12 = cSVManager.getCSVHeaders();
                    n2 = cSVManager.getRowCount();
                }
                catch (Exception var21_20) {
                    if (var21_20.getMessage() != null && var21_20.getMessage().length() != 0) {
                        arrobject[0] = var21_20.getMessage();
                        string13 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_IMPORT_VALIDATION_FAILED", (Object[])arrobject);
                        m_logger.error(string13);
                    }
                    n = -1;
                }
                finally {
                    if (n == -1) {
                        arrobject[0] = string3;
                        string13 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_IMPORT_VALIDATION_FAILED", (Object[])arrobject);
                        System.err.println(string13);
                        m_logger.error(string13);
                    }
                }
            }
            if (m_shutdownHdlr.isShutdown()) {
                n = -1;
            } else if (n != -1) {
                try {
                    SOAPImpRequestManager sOAPImpRequestManager = new SOAPImpRequestManager(m_mgrSession.getURLSessionId(), string8, string9);
                    do {
                        HashMap hashMap3 = sOAPImpRequestManager.sendImportCreateRequest(string12, hashMap2, string4, string, hashMap, string6, string7, n2);
                        string10 = (String)hashMap3.get("Status");
                        string11 = (String)hashMap3.get("ErrorMessage");
                        m_strRequestId = (String)hashMap3.get("RequestId");
                        if ("OK".equals(string10) && m_strRequestId != null && m_strRequestId.length() > 0) {
                            string13 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_IMPORT_VALIDATION_PASSED");
                            System.out.println(string13);
                            m_logger.info(string13);
                            n = 0;
                            break block18;
                        }
                        if (!"SOAPRATELIMITERR".equals(string10)) break;
                        m_logger.debug("BulkOpsClient.sendValidationRequest(): Experienced SOAP Request Rate Limit error while sending the validation request.  Will try to send again in 1 sec.");
                        Thread.sleep(1000);
                    } while (true);
                    arrobject[0] = string11;
                    string13 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_IMPORT_VALIDATION_FAILED", (Object[])arrobject);
                    System.err.println(string13);
                    m_logger.error(string13);
                    n = -1;
                }
                catch (ServiceException var21_22) {
                    string13 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_SOAP_INIT_EXCEPTION");
                    System.err.println(string13);
                    m_logger.error(string13);
                    n = -2;
                }
                catch (InterruptedException var21_23) {
                    m_logger.debug("BulkOpsClient.sendValidationRequest(): Interrupted.");
                }
            }
        }
        m_logger.debug("BulkOpsClient.sendValidationRequest(): Execution complete.");
        return n;
    }

    private static String convertFirstLetterToUpperCases(String string) {
        String[] arrstring = string.split(" ");
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < arrstring.length; ++i) {
            String string2 = arrstring[i].substring(0, 1).toUpperCase() + arrstring[i].substring(1);
            stringBuffer.append(string2);
            stringBuffer.append(" ");
        }
        return stringBuffer.toString().trim();
    }

    private static void waitForImportCompletion(String string, String string2, int n, boolean bl, String string3, String string4) {
        String string5;
        Object object = "";
        float f = 0.0f;
        int n2 = 0;
        SOAPImpRequestManager sOAPImpRequestManager = null;
        if (m_dataLoaderSessionKeeper != null && m_dataLoaderSessionKeeper.isAlive()) {
            m_dataLoaderSessionKeeper.interrupt();
        }
        if (m_logger != null) {
            m_logger.debug("BulkOpsClient.waitForImportCompletion(): Entering.");
        }
        if (bl) {
            string5 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_WAITFORUSERTOSHUTDOWN");
            System.out.println(string5);
            if (m_logger != null) {
                m_logger.info(string5);
            }
        } else {
            string5 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_IMP_PROC_STAT_CHK_START");
            System.out.println(string5);
            if (m_logger != null) {
                m_logger.info(string5);
            }
        }
        if (bl) {
            n = 1;
        }
        while (!m_shutdownHdlr.isShutdown() && !Thread.currentThread().isInterrupted()) {
            block35 : {
                if (!bl) {
                    try {
                        Object object2;
                        if (sOAPImpRequestManager == null) {
                            sOAPImpRequestManager = new SOAPImpRequestManager(string, string3, string4);
                        }
                        if (m_logger != null) {
                            m_logger.debug("BulkOpsClient.waitForImportCompletion(): Submitting BulkOpImportGetRequestDetail WS call");
                        }
                        HashMap hashMap = sOAPImpRequestManager.sendImportGetRequestDetail(string2);
                        if (m_logger != null) {
                            m_logger.debug("BulkOpsClient.waitForImportCompletion(): BulkOpImportGetRequestDetail WS call finished");
                        }
                        String string6 = (String)hashMap.get("Status");
                        if (m_logger != null) {
                            m_logger.debug("BulkOpsClient.waitForImportCompletion(): SOAP response status code=" + string6);
                        }
                        if ("OK".equals(string6)) {
                            Object[] arrobject;
                            object2 = (String)hashMap.get("RequestStatus");
                            int n3 = ((BigDecimal)hashMap.get("NumberProcessed")).intValue();
                            int n4 = ((BigDecimal)hashMap.get("NumberSubmitted")).intValue();
                            if (m_logger != null) {
                                m_logger.debug("BulkOpsClient.waitForImportCompletion(): Request Status=" + (String)object2);
                                m_logger.debug("BulkOpsClient.waitForImportCompletion(): Number Processed=" + n3);
                                m_logger.debug("BulkOpsClient.waitForImportCompletion(): Number Submitted=" + n4);
                            }
                            if (object2.equals("Completed")) {
                                arrobject = new String[]{(String)object2};
                                string5 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_IMP_PROC_STAT", (Object[])arrobject);
                                System.out.println(string5);
                                if (m_logger == null) break;
                                m_logger.info(string5);
                                break;
                            }
                            if (object2.equals("Completed with Errors") || object2.equals("Error") || object2.equals("Deleted") || object2.equals("Cancelled")) {
                                bl = true;
                                n = 1;
                                arrobject = new String[]{(String)object2};
                                string5 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_IMP_PROC_STAT", (Object[])arrobject);
                                System.out.println(string5);
                                if (m_logger != null) {
                                    m_logger.info(string5);
                                }
                                string5 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_WAITFORUSERTOSHUTDOWN");
                                System.out.println(string5);
                                if (m_logger != null) {
                                    m_logger.info(string5);
                                }
                            } else if (n3 > n2) {
                                float f2 = (float)(n3 * 10000 / n4) / 100.0f;
                                if (f2 > f) {
                                    f = f2;
                                    Object[] arrobject2 = new String[1];
                                    StringBuilder stringBuilder = new StringBuilder();
                                    Formatter formatter = new Formatter(stringBuilder, Locale.US);
                                    formatter.format("%1$.2f%%", Float.valueOf(f2));
                                    arrobject2[0] = formatter.toString();
                                    string5 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_IMP_PROC_STAT", (Object[])arrobject2);
                                    System.out.println(string5);
                                    if (m_logger != null) {
                                        m_logger.info(string5);
                                    }
                                }
                                object = object2;
                                n2 = n3;
                            }
                        } else {
                            object2 = new String[]{(String)hashMap.get("ErrorMessage")};
                            string5 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_IMP_PROC_STAT_CHK_ERR_MSG", (Object[])object2);
                            System.err.println(string5);
                            if (m_logger != null) {
                                m_logger.error(string5);
                            }
                        }
                    }
                    catch (ServiceException var11_12) {
                        if (m_logger != null) {
                            m_logger.debug("BulkOpsClient.waitForImportCompletion(): An exception ocurred while initializing the soap request manager to get request status.");
                        }
                        string5 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_IMP_PROC_STAT_CHK_ERR");
                        System.err.println(string5);
                        if (m_logger != null) {
                            m_logger.error(string5);
                        }
                        sOAPImpRequestManager = null;
                    }
                    catch (Throwable var11_13) {
                        if (m_logger != null) {
                            m_logger.debug("BulkOpsClient.waitForImportCompletion(): An exception ocurred while checking Import Request Process status.");
                        }
                        string5 = BulkOpsClientLocalizationKit.getMsg((String)"OD_BULKOPS_IMP_PROC_STAT_CHK_ERR");
                        System.err.println(string5);
                        if (m_logger == null) break block35;
                        m_logger.error(string5);
                        return;
                    }
                }
            }
            try {
                if (m_logger != null) {
                    m_logger.debug("BulkOpsClient.waitForImportCompletion(): Going to sleep for " + n + " seconds.");
                }
                Thread.sleep(n * 1000);
                continue;
            }
            catch (InterruptedException var11_14) {
                if (m_logger == null) break;
                m_logger.debug("BulkOpsClient.waitForImportCompletion(): Interrupted.");
                break;
            }
        }
        if (m_logger != null) {
            m_logger.debug("BulkOpsClient.waitForImportCompletion(): Exiting.");
        }
    }

    static {
        m_bWaitForCompletionFlag = true;
    }
}
 

