package florist;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class ReadProperties {

	static Logger logger = Logger.getLogger(ReadProperties.class);
	
	public ReadProperties() {
		PropertyConfigurator.configure("log4j.properties");
	}

	/* Reading values from Property file */
	public static HashMap ReadPropertiesFile(String PropFile) throws IOException, FileNotFoundException,
			NullPointerException {
		HashMap hashmap = new HashMap();
		try {
			logger.debug("Reading CRM properties from Property file ...");
			Properties props = new Properties();
			props.load(new FileInputStream(PropFile));
			Enumeration elements = props.propertyNames();

			hashmap.put("UploadFileLocation", props.get("UploadFileLocation"));
			hashmap.put("ErrorFileLocation", props.get("ErrorFileLocation"));
			hashmap.put("CompletedFileLocation", props.get("CompletedFileLocation"));
			hashmap.put("CRMODUrl", props.get("CRMODUrl"));
			hashmap.put("CRMODUsername", props.get("CRMODUsername"));
			hashmap.put("CRMODPassword", props.get("CRMODPassword"));
			hashmap.put("FileDelimiter", props.get("FileDelimiter"));
			hashmap.put("UseProxy", props.get("UseProxy"));
			hashmap.put("ProxyHost", props.get("ProxyHost"));
			hashmap.put("ProxyPort", props.get("ProxyPort"));
			hashmap.put("ProxyUser", props.get("ProxyUser"));
			hashmap.put("ProxyPassword", props.get("ProxyPassword"));
			
			hashmap.put("MappingFilesLocation", props.get("MappingFilesLocation"));
			hashmap.put("mtdsending.UploadFileLocation", props.get("mtdsending.UploadFileLocation"));
			hashmap.put("mtdsending.ErrorFileLocation", props.get("mtdsending.ErrorFileLocation"));
			hashmap.put("mtdsending.CompletedFileLocation", props.get("mtdsending.CompletedFileLocation"));
			hashmap.put("mtdsending.FileDelimiter", props.get("mtdsending.FileDelimiter"));
			hashmap.put("mtdsending.mapFile.currentyear", props.get("mtdsending.mapFile.currentyear"));
			hashmap.put("mtdsending.mapFile.prioryear", props.get("mtdsending.mapFile.prioryear"));
			
			hashmap.put("cshsummary.UploadFileLocation", props.get("cshsummary.UploadFileLocation"));
			hashmap.put("cshsummary.ErrorFileLocation", props.get("cshsummary.ErrorFileLocation"));
			hashmap.put("cshsummary.CompletedFileLocation", props.get("cshsummary.CompletedFileLocation"));
			hashmap.put("cshsummary.FileDelimiter", props.get("cshsummary.FileDelimiter"));
			hashmap.put("cshsummary.mapFile", props.get("cshsummary.mapFile"));
			
			hashmap.put("blk.UploadFileLocation", props.get("blk.UploadFileLocation"));
			hashmap.put("blk.ErrorFileLocation", props.get("blk.ErrorFileLocation"));
			hashmap.put("blk.CompletedFileLocation", props.get("blk.CompletedFileLocation"));
			hashmap.put("blk.FileDelimiter", props.get("blk.FileDelimiter"));
			hashmap.put("blk.mappingfile", props.get("blk.mappingfile"));
			
			hashmap.put("cp.UploadFileLocation", props.get("cp.UploadFileLocation"));
			hashmap.put("cp.ErrorFileLocation", props.get("cp.ErrorFileLocation"));
			hashmap.put("cp.CompletedFileLocation", props.get("cp.CompletedFileLocation"));
			hashmap.put("cp.FileDelimiter", props.get("cp.FileDelimiter"));
			hashmap.put("cp.mappingfileupdateshop", props.get("cp.mappingfileupdateshop"));
			hashmap.put("cp.mappingfilenewshop", props.get("cp.mappingfilenewshop"));
			hashmap.put("cp.mappingfilelifecycle", props.get("cp.mappingfilelifecycle"));

			
			logger.debug("UploadFileLocation: " + hashmap.get("UploadFileLocation").toString());
			logger.debug("ErrorFileLocation: " + hashmap.get("ErrorFileLocation").toString());
			logger.debug("CompletedFileLocation: "+ hashmap.get("CompletedFileLocation").toString());
			logger.debug("CRMODUrl: "+ hashmap.get("CRMODUrl").toString());
			logger.debug("CRMODUsername: "+ hashmap.get("CRMODUsername").toString());
			logger.debug("FileDelimiter: "+ hashmap.get("FileDelimiter").toString());
			logger.debug("UseProxy: "+ hashmap.get("UseProxy").toString());
			
			} catch (Exception ex) {
			throw new RuntimeException("Unable to read property file: "+ ex.toString());
		}
		return hashmap;
	}

}
