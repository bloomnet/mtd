package net.bloomnet.poi.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;


/*
 * Helper functions to aid in the management of sheets
 */
public class SheetUtil extends Object {

	
	/**
	 * Given a sheet, this method deletes a column from a sheet and moves
	 * all the columns to the right of it to the left one cell.
	 * 
	 * Note, this method will not update any formula references.
	 * 
	 * @param sheet
	 * @param column
	 */
	public static void deleteColumn( Sheet sheet, int columnToDelete ){
		int maxColumn = 0;
		for ( int r=0; r < sheet.getLastRowNum()+1; r++ ){
			Row	row	= sheet.getRow( r );
			
			// if no row exists here; then nothing to do; next!
			if ( row == null )
				continue;
			
			// if the row doesn't have this many columns then we are good; next!
			int lastColumn = row.getLastCellNum();
			if ( lastColumn > maxColumn )
				maxColumn = lastColumn;
			
			if ( lastColumn < columnToDelete )
				continue;
			
			for ( int x=columnToDelete+1; x < lastColumn + 1; x++ ){
				Cell oldCell	= row.getCell(x-1);
				if ( oldCell != null )
					row.removeCell( oldCell );
				
				Cell nextCell	= row.getCell( x );
				if ( nextCell != null ){
					Cell newCell	= row.createCell( x-1, nextCell.getCellType() );
					cloneCell(newCell, nextCell);
				}
			}
		}

		
		// Adjust the column widths
		for ( int c=0; c < maxColumn; c++ ){
			sheet.setColumnWidth( c, sheet.getColumnWidth(c+1) );
		}
	}
	
	public static void insertColumn( Sheet sheet, int columnToInsert ){
		int maxColumn = 0;
		
		for ( int r=0; r < sheet.getLastRowNum()+1; r++ ){
			Row	row	= sheet.getRow( r );

			// if no row exists here; then nothing to do; next!
			if ( row == null )
				continue;
			
			// if the row doesn't have this many columns then we are good; next!
			int lastColumn = row.getLastCellNum();
			if ( lastColumn > maxColumn )
				maxColumn = lastColumn;
			
			if ( lastColumn < columnToInsert )
				continue;
			
			for ( int x=lastColumn; x >=columnToInsert; x-- ){
				Cell oldCell = row.getCell(x);
				Cell newCell = null;
				if(oldCell==null)
					continue;
				newCell = row.createCell(x+1,oldCell.getCellType());
				
				cloneCell(newCell, oldCell);
				oldCell.setCellValue("");
				}
			}
		

		
		// Adjust the column widths
		//for ( int c=0; c < maxColumn; c++ ){
		//	sheet.setColumnWidth( c, sheet.getColumnWidth(c+1) );
		//}
	}
	
	public static void copyColumns( Sheet sheet, int source, int dest , boolean omitHeader){
		
		for ( int r=0; r < sheet.getLastRowNum()+1; r++ ){
			
			if(omitHeader && r==0)
				continue;
			
			Row	row	= sheet.getRow( r );
			
			Cell sourceCell = row.getCell(source);
			Cell destCell = row.getCell(dest);
			cloneCell(destCell, sourceCell);
			
		}
	}
	
	
	/*
	 * Takes an existing Cell and merges all the styles and forumla
	 * into the new one
	 */
	private static void cloneCell( Cell cNew, Cell cOld ){
		cNew.setCellComment( cOld.getCellComment() );
		cNew.setCellStyle( cOld.getCellStyle() );
		
		switch ( cOld.getCellType() ){
			case Cell.CELL_TYPE_BOOLEAN:{
				cNew.setCellValue( cOld.getBooleanCellValue() );
				break;
			}
			case Cell.CELL_TYPE_NUMERIC:{
				cNew.setCellValue( cOld.getNumericCellValue() );
				break;
			}
			case Cell.CELL_TYPE_STRING:{
				cNew.setCellValue( cOld.getStringCellValue() );
				break;
			}
			case Cell.CELL_TYPE_ERROR:{
				cNew.setCellValue( cOld.getErrorCellValue() );
				break;
			}
			case Cell.CELL_TYPE_FORMULA:{
				cNew.setCellFormula( cOld.getCellFormula() );
				break;
			}
		}
		
	}
	
	public static void setColumnToIncrement(Sheet sheet, int col, String prefix, boolean omitHeader){
		
		
		for ( int r=0; r < sheet.getLastRowNum()+1; r++ ){
			
			if(omitHeader && r==0)
				continue;
			
			Row	row	= sheet.getRow( r );
			
			Cell sourceCell = row.getCell(col);
			if(sourceCell==null)
				sourceCell = row.createCell(col);
			
				sourceCell.setCellValue(prefix+String.format("%03d", r));
			
		}
	}
	
	public static void setColumnToText(Sheet sheet, int col, String val, boolean omitHeader){
		
		for ( int r=0; r < sheet.getLastRowNum()+1; r++ ){
			
			if(omitHeader && r==0)
				continue;
			
			Row	row	= sheet.getRow( r );
			
			Cell sourceCell = row.getCell(col);
			if(sourceCell==null)
				sourceCell = row.createCell(col);
			
				sourceCell.setCellValue(val);
			
		}
	}
	
	//XSSFDataFormat df = workBook.createDataFormat();
	//cs.setDataFormat(df.getFormat("d-mmm-yy"));
	//cell.set
	
	public static void setColumnDateFormat(Sheet sheet, int col, String format, boolean omitHeader){
		
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");; 
		
		
		
		for ( int r=0; r < sheet.getLastRowNum()+1; r++ ){
			
			if(omitHeader && r==0)
				continue;
			
			Row	row	= sheet.getRow( r );
			
			Cell sourceCell = row.getCell(col);
			if(sourceCell==null)
				continue;
			
			try{
				if(DateUtil.isCellDateFormatted(sourceCell)){
					
					Date date = sourceCell.getDateCellValue();
	
					sourceCell.setCellValue(formatter.format(date));
					//System.out.println(sourceCell.getStringCellValue());
				}
				else{
					//NOTHING!
				}
			}catch(Exception e){//do nothing}
			}
			
		}
		
		
	}

}