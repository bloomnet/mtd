<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE HTML >
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>CRM Florist Information Form</title>
</head>
     
<body>
  <h2>Enter florist information below:</h2>
  <form method="get">
    <input type="checkbox" name="choice" value="1">1
    <input type="checkbox" name="choice" value="2">2
    <input type="checkbox" name="choice" value="3">3
    <input type="checkbox" name="choice" value="4">4
    <p>
    Shop Code: <input type="text" name="scode" value="">
    <p>
    Notes: <input type="text" name="notes" value="">
    <p>
    <input type="submit" value="Submit">
  </form>
  
  <% 
  String scode = request.getParameter("scode");
  String notes = request.getParameter("notes");
  String[] choices = request.getParameterValues("choice");
  if (scode != null) {
  %>
  	<h4>Selected shop code:</h4> 
  	<%=scode %>
  	
  	<h4>Notes:</h4> 
  	<%=notes %>
  	
  
    <h4>Selected checkbox value(s):</h4>
    <ul>
      <%
      for (String choice : choices) { 
      %>
        <li><%= choice %></li>
      <%
      }
      %>
    </ul>
  <%
  }
  %>
  <br /><a href="<%= request.getRequestURI() %>">Enter Information Again</a> 
<body>
</html>